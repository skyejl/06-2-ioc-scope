package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class IocScopeTest {
    private AnnotationConfigApplicationContext context;
    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    @Test
    void should_get_same_instance_with_InterfaceOneImpl_and_InterfaceOne() {
//        InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
//        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
//        assertSame(interfaceOneImpl, interfaceOne);
    }

    @Test
    void should_not_same_instance_with_InterfaceOneImpl_and_interfaceOneImplChild() {
        interfaceOneImplChild interfaceOneImplChild = context.getBean(interfaceOneImplChild.class);
        assertThrows(NoUniqueBeanDefinitionException.class, () -> {
            InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
            assertNotSame(interfaceOneImpl, interfaceOneImplChild);
        });
    }

    @Test
    void should_get_same_instance_with_DerivedClass_and_AbstractBaseClass() {
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        assertSame(derivedClass, abstractBaseClass);
    }

    @Test
    void should_get_different_instances_with_Prototype() {
        SimplePrototypeScopeClass bean_one = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean_two = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(bean_one, bean_two);
    }

    @Test
    void should_create_singleton_once_and_create_prototype_twice_with_prototype_dependency_singleton() {
        PrototypeScopeDependsOnSingleton bean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        context.getBean(PrototypeScopeDependsOnSingleton.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonDependent = logs.stream().filter(log -> log.equals("SingletonDependent")).collect(Collectors.toList());
        List<String> prototypeScopeDependsOnSingleton = logs.stream().filter(log -> log.equals("PrototypeScopeDependsOnSingleton")).collect(Collectors.toList());

        assertEquals(1, singletonDependent.size());
        assertEquals(2, prototypeScopeDependsOnSingleton.size());
    }

    @Test
    void should_create_both_singleton_and_prototype_once_with_singleton_dependency_prototype() {
        SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
        context.getBean(SingletonDependsOnPrototype.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> prototypeDependent = logs.stream().filter(log -> log.equals("PrototypeDependent")).collect(Collectors.toList());
        List<String> singletonDependsOnPrototype = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototype")).collect(Collectors.toList());

        assertEquals(1, prototypeDependent.size());
        assertEquals(1, singletonDependsOnPrototype.size());
    }

    @Test
    void should_create_PrototypeDependentWithProxy_instances_when_call_its_field() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().field();
        bean.getPrototypeDependentWithProxy().field();
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonDependsOnPrototypeProxy = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototypeProxy")).collect(Collectors.toList());
        List<String> prototypeDependentWithProxy = logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).collect(Collectors.toList());

        assertEquals(1,singletonDependsOnPrototypeProxy.size());
        assertEquals(2,prototypeDependentWithProxy.size());
    }

    @Test
    void should_create_PrototypeDependentWithProxy_instances_twice() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.prototypeProxyNotImplementIndependent();
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonDependsOnPrototypeProxyBatchCall = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototypeProxyBatchCall")).collect(Collectors.toList());
        List<String> prototypeDependentWithProxy = logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).collect(Collectors.toList());

        assertEquals(1,singletonDependsOnPrototypeProxyBatchCall.size());
        assertEquals(2,prototypeDependentWithProxy.size());

    }
}
