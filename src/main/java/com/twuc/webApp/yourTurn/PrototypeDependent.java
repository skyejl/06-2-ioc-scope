package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeDependent {
    private  ScopeLogger logger;

    public PrototypeDependent(ScopeLogger logger) {
        logger.setLog("PrototypeDependent");
        this.logger = logger;
    }
    public ScopeLogger getLogger() {
        return logger;
    }
}
