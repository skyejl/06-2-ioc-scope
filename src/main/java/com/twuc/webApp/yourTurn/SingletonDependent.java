package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private final ScopeLogger logger;

    public SingletonDependent(ScopeLogger logger) {
        logger.setLog("SingletonDependent");
        this.logger = logger;
    }
    public ScopeLogger getLogger() {
        return logger;
    }
}
