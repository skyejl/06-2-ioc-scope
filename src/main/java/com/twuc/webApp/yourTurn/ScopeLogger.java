package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class ScopeLogger {
    private List logs = new ArrayList<String>();
    public void setLog(String log) {
        this.logs.add(log);
    }
    public List getLogs() {
        return logs;
    }
}
