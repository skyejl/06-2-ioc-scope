package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScope {
    private final ScopeLogger logger;
    public PrototypeScope(ScopeLogger logger) {
        logger.setLog("PrototypeScope");
        this.logger = logger;
    }

    public ScopeLogger getLogger() {
        return this.logger;
    }
}
