package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private  ScopeLogger logger;
    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy, ScopeLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        logger.setLog("SingletonDependsOnPrototypeProxy");
        this.logger = logger;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public ScopeLogger getLogger() {
        return logger;
    }
}
