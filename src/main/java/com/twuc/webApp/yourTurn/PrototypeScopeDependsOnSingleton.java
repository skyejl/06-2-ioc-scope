package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {
    private SingletonDependent singletonDependent;
    private ScopeLogger logger;
    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent, ScopeLogger logger) {
        this.singletonDependent = singletonDependent;
        logger.setLog("PrototypeScopeDependsOnSingleton");
        this.logger = logger;
    }

    public ScopeLogger getLogger() {
        return logger;
    }



}
