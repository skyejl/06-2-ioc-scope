package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {
    private PrototypeDependent prototypeDependent;
    private final ScopeLogger logger;
    public SingletonDependsOnPrototype(PrototypeDependent prototypeDependent, ScopeLogger logger) {
        this.prototypeDependent = prototypeDependent;
        logger.setLog("SingletonDependsOnPrototype");
        this.logger = logger;
    }

    public ScopeLogger getLogger() {
        return logger;
    }
}
