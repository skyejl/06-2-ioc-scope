package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@Component
@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
public class PrototypeDependentWithProxy {
    private  ScopeLogger logger;

    public PrototypeDependentWithProxy(ScopeLogger logger) {
        logger.setLog("PrototypeDependentWithProxy");
        this.logger = logger;
    }
    public ScopeLogger getLogger() {
        return logger;
    }
    public void field() {
    }

}
