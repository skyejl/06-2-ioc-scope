package com.twuc.webApp.yourTurn;

public class SingletonScope {
    private final ScopeLogger logger;
    public SingletonScope(ScopeLogger logger) {
        logger.setLog("SingletonScope");
        this.logger = logger;
    }

    public ScopeLogger getLogger() {
        return this.logger;
    }
}
