package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private  ScopeLogger logger;
    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy prototypeDependentWithProxy, ScopeLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        logger.setLog("SingletonDependsOnPrototypeProxyBatchCall");
        this.logger = logger;
    }

    public void prototypeProxyNotImplementIndependent() {
        this.getPrototypeDependentWithProxy().toString();
        this.getPrototypeDependentWithProxy().toString();
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public ScopeLogger getLogger() {
        return logger;
    }
}
